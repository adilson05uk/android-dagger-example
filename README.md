# Dagger 2 Android Application Example

This project demonstrates how to setup and use the Dagger 2 dependency injection framework in an Android application.

Refer to the application's [build.gradle](app/build.gradle) file for the required dependencies.

Refer to the [ApplicationComponent](app/src/main/java/com/masabi/dagger/ApplicationComponent.java) interface for the bridge between the Modules in the application and the Activity class that the Component injects into. This Component class also makes some objects in the object graph available to the project's instrumentation tests.

Refer to the [androidTest](app/src/androidTest) folder for examples of Android instrumentation tests that make use of the [ApplicationComponent](app/src/main/java/com/masabi/dagger/ApplicationComponent.java) interface via the [MyApplication](app/src/main/java/com/masabi/dagger/MyApplication.java) singleton instance.

Refer to the [test](app/src/test) folder for an example of a bog standard Mockito-assisted unit test.