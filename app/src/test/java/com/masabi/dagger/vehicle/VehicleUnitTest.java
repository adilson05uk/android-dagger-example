package com.masabi.dagger.vehicle;

import com.masabi.dagger.vehicle.motor.Motor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class VehicleUnitTest {

    @Test
    public void test_accelerate() {
        // Given.
        Motor motor = mock(Motor.class);
        Vehicle vehicle = new Vehicle(motor);

        // When.
        vehicle.accelerate();

        // Then.
        verify(motor, times(1)).accelerate();
    }

    @Test
    public void test_decelerate() {
        // Given.
        Motor motor = mock(Motor.class);
        Vehicle vehicle = new Vehicle(motor);

        // When.
        vehicle.decelerate();

        // Then.
        verify(motor, times(1)).decelerate();
    }
}