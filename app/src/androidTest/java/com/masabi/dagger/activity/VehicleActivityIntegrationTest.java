package com.masabi.dagger.activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.masabi.dagger.R;
import com.masabi.dagger.vehicle.Vehicle;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class VehicleActivityIntegrationTest {

    @Rule
    public ActivityTestRule<VehicleActivity> activityTestRule = new ActivityTestRule<>(VehicleActivity.class);

    @Test
    public void test_accelerate() {
        // Given.
        Vehicle vehicle = getTargetActivity().getVehicle();
        int vehicleRPMBefore = vehicle.getRPM();

        // And.
        onView(withId(R.id.textViewRPM)).check(matches(withText(String.valueOf(vehicleRPMBefore))));

        // When.
        onView(withId(R.id.buttonAccelerate)).perform(click());

        // Then.
        int vehicleRPMAfter = vehicle.getRPM();
        assertThat(vehicleRPMAfter, is(greaterThan(vehicleRPMBefore)));

        // And.
        onView(withId(R.id.textViewRPM)).check(matches(withText(String.valueOf(vehicleRPMAfter))));
    }

    @Test
    public void test_decelerate() {
        // Given.
        onView(withId(R.id.buttonAccelerate)).perform(click());

        Vehicle vehicle = getTargetActivity().getVehicle();
        int vehicleRPMBefore = vehicle.getRPM();

        // When.
        onView(withId(R.id.buttonDecelerate)).perform(click());

        // Then.
        int vehicleRPMAfter = vehicle.getRPM();
        assertThat(vehicleRPMAfter, is(lessThan(vehicleRPMBefore)));

        // And.
        onView(withId(R.id.textViewRPM)).check(matches(withText(String.valueOf(vehicleRPMAfter))));
    }

    private VehicleActivity getTargetActivity() {
        return activityTestRule.getActivity();
    }
}
