package com.masabi.dagger.storage;

import android.support.test.runner.AndroidJUnit4;

import com.masabi.dagger.MyApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class VehicleStorageIntegrationTest {

    private VehicleStorage target;

    @Before
    public void setUp() {
        target = MyApplication.getInstance().getApplicationComponent().getVehicleStorage();
    }

    @Test
    public void test_getVehicleRPM_after_saveVehicleRPM() {
        // Given.
        int input = 4;
        target.saveVehicleRPM(input);

        //When.
        int output = target.getVehicleRPM();

        // Then.
        assertEquals(input, output);
    }
}
