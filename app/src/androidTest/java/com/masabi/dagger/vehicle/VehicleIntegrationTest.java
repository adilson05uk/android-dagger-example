package com.masabi.dagger.vehicle;

import android.support.test.runner.AndroidJUnit4;

import com.masabi.dagger.MyApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

@RunWith(AndroidJUnit4.class)
public class VehicleIntegrationTest {

    private Vehicle vehicle;

    @Before
    public void setUp() {
        Vehicle.Factory vehicleFactory = MyApplication.getInstance().getApplicationComponent().getVehicleFactory();
        vehicle = vehicleFactory.create(40);
    }

    @Test
    public void test_accelerate() {
        // Given.
        int vehicleRPMBefore = vehicle.getRPM();

        // When.
        vehicle.accelerate();

        // Then.
        int vehicleRPMAfter = vehicle.getRPM();
        assertThat(vehicleRPMAfter, is(greaterThan(vehicleRPMBefore)));
    }

    @Test
    public void test_decelerate() {
        // Given.
        int vehicleRPMBefore = vehicle.getRPM();

        // When.
        vehicle.decelerate();

        // Then.
        int vehicleRPMAfter = vehicle.getRPM();
        assertThat(vehicleRPMAfter, is(lessThan(vehicleRPMBefore)));
    }
}
