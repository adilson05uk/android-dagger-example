package com.masabi.dagger.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.masabi.dagger.MyApplication;
import com.masabi.dagger.R;
import com.masabi.dagger.storage.VehicleStorage;
import com.masabi.dagger.vehicle.Vehicle;

import javax.inject.Inject;

public class VehicleActivity extends AppCompatActivity {

    private TextView textViewVehicleRPM;

    @Inject
    Vehicle.Factory vehicleFactory;

    @Inject
    VehicleStorage vehicleStorage;

    private Vehicle vehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);

        MyApplication.getInstance().getApplicationComponent().inject(this);

        int vehicleRPM = vehicleStorage.getVehicleRPM();
        vehicle = vehicleFactory.create(vehicleRPM);

        // initialise views

        textViewVehicleRPM = (TextView) findViewById(R.id.textViewRPM);
        Button buttonAccelerate = (Button) findViewById(R.id.buttonAccelerate);
        Button buttonDecelerate = (Button) findViewById(R.id.buttonDecelerate);

        // set click listeners

        buttonAccelerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accelerateVehicle();
            }
        });

        buttonDecelerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decelerateVehicle();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayVehicleRPM();
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    private void displayVehicleRPM() {
        int vehicleRPM = vehicle.getRPM();
        textViewVehicleRPM.setText(String.valueOf(vehicleRPM));
    }

    private void accelerateVehicle() {
        vehicle.accelerate();
        vehicleStorage.saveVehicleRPM(vehicle.getRPM());
        displayVehicleRPM();
    }

    private void decelerateVehicle() {
        vehicle.decelerate();
        vehicleStorage.saveVehicleRPM(vehicle.getRPM());
        displayVehicleRPM();
    }
}
