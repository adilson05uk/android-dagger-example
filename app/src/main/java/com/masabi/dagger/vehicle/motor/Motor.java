package com.masabi.dagger.vehicle.motor;

public class Motor {

    private int rpm;

    private Motor(int rpm) {
        this.rpm = rpm;
    }

    public int getRpm() {
        return rpm;
    }

    public void accelerate() {
        rpm += 4;
    }

    public void decelerate() {
        rpm = Math.max(rpm - 4, 0);
    }

    public static class Factory {

        public Motor create(int rpm) {
            return new Motor(rpm);
        }
    }
}
