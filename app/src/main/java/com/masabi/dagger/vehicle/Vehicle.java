package com.masabi.dagger.vehicle;

import android.support.annotation.NonNull;

import com.masabi.dagger.vehicle.motor.Motor;

import javax.inject.Inject;

public class Vehicle {

    @NonNull
    private final Motor motor;

    Vehicle(@NonNull Motor motor) {
        this.motor = motor;
    }

    public int getRPM(){
        return motor.getRpm();
    }

    public void accelerate(){
        motor.accelerate();
    }

    public void decelerate(){
        motor.decelerate();
    }

    public static class Factory {

        @NonNull
        private final Motor.Factory motorFactory;

        @Inject
        Factory(@NonNull Motor.Factory motorFactory) {
            this.motorFactory = motorFactory;
        }

        @NonNull
        public Vehicle create(int rpm) {
            Motor motor = motorFactory.create(rpm);
            return new Vehicle(motor);
        }
    }
}
