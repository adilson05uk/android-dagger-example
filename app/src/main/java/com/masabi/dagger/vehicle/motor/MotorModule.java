package com.masabi.dagger.vehicle.motor;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MotorModule {

    @Provides
    @Singleton
    @NonNull
    Motor.Factory provideMotorFactory(){
        return new Motor.Factory();
    }
}
