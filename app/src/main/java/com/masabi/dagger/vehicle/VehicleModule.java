package com.masabi.dagger.vehicle;

import android.support.annotation.NonNull;

import com.masabi.dagger.vehicle.motor.Motor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class VehicleModule {

    @Provides
    @Singleton
    @NonNull
    Vehicle.Factory provideVehicleFactory(Motor.Factory motorFactory){
        return new Vehicle.Factory(motorFactory);
    }
}
