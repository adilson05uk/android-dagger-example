package com.masabi.dagger;

import com.masabi.dagger.activity.VehicleActivity;
import com.masabi.dagger.storage.StorageModule;
import com.masabi.dagger.storage.VehicleStorage;
import com.masabi.dagger.vehicle.Vehicle;
import com.masabi.dagger.vehicle.VehicleModule;
import com.masabi.dagger.vehicle.motor.MotorModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        StorageModule.class,
        MotorModule.class,
        VehicleModule.class
})
public interface ApplicationComponent {

    void inject(VehicleActivity activity);

    /**
     * Getter method for object access in the instrumentation tests.
     */
    VehicleStorage getVehicleStorage();

    /**
     * Getter method for object access in the instrumentation tests.
     */
    Vehicle.Factory getVehicleFactory();
}
