package com.masabi.dagger.storage;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class VehicleStorage {

    @NonNull
    private final SharedPreferences sharedPreferences;

    VehicleStorage(@NonNull SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public int getVehicleRPM() {
        return sharedPreferences.getInt("VehicleRPM", 0);
    }

    public void saveVehicleRPM(int vehicleRPM) {
        sharedPreferences.edit().putInt("VehicleRPM", vehicleRPM).apply();
    }
}
