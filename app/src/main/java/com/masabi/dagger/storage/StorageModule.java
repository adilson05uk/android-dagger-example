package com.masabi.dagger.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    @NonNull
    SharedPreferences provideSharedPreferencesForVehicleInfo(Context applicationContext) {
        return applicationContext.getSharedPreferences("Vehicle", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    @NonNull
    VehicleStorage provideVehicleStorage(SharedPreferences sharedPreferences) {
        return new VehicleStorage(sharedPreferences);
    }
}
